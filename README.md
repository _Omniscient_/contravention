# Project aims

This project aims is to integrate adobe xd template into flutter code.

You will find the template in the root folder. It has an extension of .xd

## Getting Started

You can use this code as you want.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://docs.flutter.dev/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://docs.flutter.dev/cookbook)

For help getting started with Flutter development, view the
[online documentation](https://docs.flutter.dev/), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

## Installation

👉🏿 git clone http_or_ssh_link

👉🏿 flutter pub get

run the app

That is it 🥸🤗
