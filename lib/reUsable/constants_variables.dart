
import 'dart:ui';

// Colors variables
const Color primaryBackground = Color(0xFFeafff7);
const Color primaryGreenColor = Color(0xFF06c37e); // for text and buttons
const Color textColor = Color(0xFF172b4d);
const Color primaryCircleAvatarColor = Color(0xFFfa6400);
const Color greyColor = Color(0xFF7a869a); // for sub text
const Color textFieldBackground = Color(0xFFf4f5f7);
const Color yellowColor = Color(0xFFffdd00);


//fonts variables
const primaryTextFont = 'Josefin_Sans';