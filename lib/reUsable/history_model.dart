

class HistoryModel {

  HistoryModel({
    this.date = '',
    required this.detail,
    required this.title,
    required this.type,
    this.logo = '',
    required this.price
  });

  String date;
  String logo;
  String title;
  String detail;
  String price;
  String type;

  static List<HistoryModel> recentHistoryList = [];
  static List<HistoryModel> allHistoryList = [];

  // we add some elements to the list above for testing purpose
  static addToRecentListForTestPurpose() {

    HistoryModel.recentHistoryList.addAll([
     HistoryModel(
         title : 'Exces de vitesse',
         detail : '01/02/2023 : 08H20',
         price : '- 2 000',
         type : 'minus'
     ),
    HistoryModel(
        logo : 'assets/imgs/orangeMoney.jpeg',
        title : 'Ajout de fonds',
        detail : '03/02/2023 : 12H20',
        price : '+ 15 000',
        type : 'add'
    ),
      HistoryModel(
        title : 'Conduite en sens inverse',
        detail : '01/02/2023 : 08H20',
        price : '- 10 000',
        type : 'minus'
      ),
    HistoryModel(
        logo : 'assets/imgs/tresorPay.png',
        title : 'Ajout de fonds',
        detail : '01/02/2023 : 08H20',
        price : '+ 6 950',
        type : 'add'
    )
    ]);

  }


  // we add some elements to the list above for testing purpose
  static addToAllListForTestPurpose() {
    HistoryModel.allHistoryList.addAll([
      HistoryModel(
          date: 'November 12, Tuesday',
          title : 'Exces de vitesse',
          detail : '01/02/2023 : 08H20',
          price : '- 2 000',
          type : 'minus'
      ),
      HistoryModel(
          date: 'November 12, Tuesday',
          logo : 'assets/imgs/orangeMoney.jpeg',
          title : 'Ajout de fonds',
          detail : '03/02/2023 : 12H20',
          price : '+ 15 000',
          type : 'add'
      ),
      HistoryModel(
          date: 'November 12, Tuesday',
          title : 'Conduite en sens inverse',
          detail : '01/02/2023 : 08H20',
          price : '- 10 000',
          type : 'minus'
      ),
      HistoryModel(
          date: 'November 06, Wednesday',
          logo : 'assets/imgs/tresorPay.png',
          title : 'Ajout de fonds',
          detail : '01/02/2023 : 08H20',
          price : '+ 6 950',
          type : 'add'
      ),
      HistoryModel(
          date: 'November 06, Wednesday',
          title : 'Conduite en sens inverse',
          detail : '01/02/2023 : 08H20',
          price : '- 10 000',
          type : 'minus'
      ),
      HistoryModel(
          date: 'November 04, Monday',
          title : 'Exces de vitesse',
          detail : '01/02/2023 : 08H20',
          price : '- 2 000',
          type : 'minus'
      ),
      HistoryModel(
          date: 'November 04, Monday',
          logo : 'assets/imgs/orangeMoney.jpeg',
          title : 'Ajout de fonds',
          detail : '03/02/2023 : 12H20',
          price : '+ 15 000',
          type : 'add'
      ),
      HistoryModel(
          date: 'November 04, Monday',
          title : 'Conduite en sens inverse',
          detail : '01/02/2023 : 08H20',
          price : '- 10 000',
          type : 'minus'
      ),
      HistoryModel(
          date: 'November 04, Monday',
          logo : 'assets/imgs/tresorPay.png',
          title : 'Ajout de fonds',
          detail : '01/02/2023 : 08H20',
          price : '+ 6 950',
          type : 'add'
      ),
    ]);
  }
}