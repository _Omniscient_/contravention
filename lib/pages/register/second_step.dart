// --custom comment-- mean comment written by developer
// because as a developer, i use intelij which automatically comment line

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:contravention/reUsable/constants_variables.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../../reUsable/slide_left_to_right.dart';
import '../login/login_screen.dart';


class RegisterSecondStep extends StatefulWidget {
  const RegisterSecondStep({super.key});

  @override
  State<RegisterSecondStep> createState() => _RegisterSecondStepState();
}

class _RegisterSecondStepState extends State<RegisterSecondStep> {

  late TextEditingController permisNumFieldController;
  late TextEditingController phoneTextFieldController;
  late TextEditingController immatriculationFieldController;
  Widget? permisNumFieldSuffix;
  Widget? immatriculationFieldSuffix;
  bool isChecked = false;
  String vehicle = 'Marque de votre véhicule';

  List<String> vehicles = ['Marque de votre véhicule', 'Mercedes', 'Toyota', 'Maserati', 'Tesla'];

  @override
  void initState() {
    permisNumFieldController = TextEditingController();
    phoneTextFieldController = TextEditingController();
    immatriculationFieldController = TextEditingController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: primaryBackground,
        leading: const Icon(FontAwesomeIcons.xmark, color: Colors.transparent,),
      ),
      backgroundColor: primaryBackground,
      body: SingleChildScrollView(
        // --Custom comment-- we use Stack here because we want
        // to freely move the avatar
        child: Stack(
          children: [
            Column(
              children: [
                Center(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      IconButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        icon: const Icon(FontAwesomeIcons.xmark, size: 16,),
                      ),
                      SizedBox(width: screenWidth * 0.1,),
                      InkWell(
                          onTap: () {
                            Navigator.push(context, SlideRightPageRoute(page: const LoginScreen()));
                          },
                        child: RichText(
                          text: const TextSpan(
                              text: 'Vous avez déjà un compte ? ',
                              style: TextStyle(
                                  color: textColor, fontSize: 14, fontFamily: primaryTextFont),
                              children: <TextSpan>[
                                TextSpan(text:'Connexion', style: TextStyle(
                                    fontWeight: FontWeight.w600, color: primaryGreenColor,
                                    fontSize: 14, fontFamily: primaryTextFont),)
                              ]
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                SizedBox(height: screenHeight * 0.056,),
                Container(
                  width: screenWidth,
                  height: screenHeight,
                  decoration: const BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(40),
                          topRight: Radius.circular(40)
                      )
                  ),
                  child: Center(
                    child: Column(
                      children: [
                        const SizedBox(height: 90,),
                        const Text(
                          'Vos informations',
                          style: TextStyle(
                              fontWeight: FontWeight.w600,
                              fontFamily: primaryTextFont, color: textColor,
                              fontSize: 24,
                              letterSpacing: 0.5
                          ),),
                        const SizedBox(height: 15,),
                        const Text(
                          'Votre permis et ajoutez un véhicule',
                          style: TextStyle(
                              fontWeight: FontWeight.w400,
                              color: greyColor
                          ),
                        ),
                        const SizedBox(height: 45,),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: screenWidth * 0.06),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const Text(
                                'Numéro du permis de conduire',
                                style: TextStyle(
                                    fontWeight: FontWeight.w500,
                                    color: greyColor,
                                    fontSize: 14
                                ),
                              ),
                              const SizedBox(height: 10,),
                              SizedBox(
                                height: 55,
                                child: CupertinoTextField(
                                  onChanged: (value) {
                                    setState(() {
                                      if(value.length >= 5) {
                                        permisNumFieldSuffix = Row(
                                          children: [
                                            Container(
                                              width: 22,
                                              height: 22,
                                              decoration: BoxDecoration(
                                                  shape: BoxShape.circle,
                                                  border: Border.all(width: 2, color: primaryGreenColor)
                                              ),
                                              child: const Icon(Icons.check, color: primaryGreenColor, size: 15,),
                                            ),
                                            const SizedBox(width: 10,),
                                          ],
                                        );
                                      } else {
                                        permisNumFieldSuffix = null;
                                      }
                                    });
                                  },
                                  controller: permisNumFieldController,
                                  prefix:  const Row(
                                    children: [
                                      SizedBox(width: 10,),
                                      Icon(Icons.numbers),
                                      SizedBox(width: 20,)
                                    ],
                                  ),
                                  suffix: permisNumFieldSuffix,
                                  placeholder: 'Numéro de permis',
                                  keyboardType: TextInputType.text,
                                  decoration: BoxDecoration(
                                      color: textFieldBackground,
                                      borderRadius: BorderRadius.circular(15)
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                        SizedBox(height: screenHeight * 0.03,),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: screenWidth * 0.06),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const Text(
                                'Votre marque de véhicule',
                                style: TextStyle(
                                    fontWeight: FontWeight.w500,
                                    color: greyColor,
                                    fontSize: 14
                                ),
                              ),
                              const SizedBox(height: 10,),
                              DecoratedBox(
                                  decoration: BoxDecoration(
                                      color: textFieldBackground,
                                      borderRadius: BorderRadius.circular(15),
                                  ),
                                  child: Padding(
                                      padding: const EdgeInsets.only(left:30, right:30),
                                      child: DropdownButton(
                                        icon: const Icon(Icons.keyboard_arrow_down),
                                        value: vehicle,
                                        items: List.generate(
                                          vehicles.length,
                                            (index) {
                                              return DropdownMenuItem(
                                                value: vehicles[index],
                                                child: Text(
                                                  vehicles[index],
                                                  style: const TextStyle(
                                                    fontFamily: primaryTextFont,
                                                    color: textColor,
                                                  ),
                                                ),
                                              );
                                            }
                                        ),
                                        onChanged: (value){
                                          setState(() {
                                            vehicle = value!;
                                          });
                                        },
                                        isExpanded: true,
                                        underline: Container(),
                                        style: const TextStyle(fontSize: 14, color: Colors.white),
                                        dropdownColor: primaryGreenColor,
                                        iconEnabledColor: textColor,
                                      )
                                  )
                              )
                            ],
                          ),
                        ),
                        SizedBox(height: screenHeight * 0.03,),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: screenWidth * 0.06),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const Text(
                                "Plaque d'immatriculation",
                                style: TextStyle(
                                    fontWeight: FontWeight.w500,
                                    color: greyColor,
                                    fontSize: 14
                                ),
                              ),
                              const SizedBox(height: 10,),
                              SizedBox(
                                height: 55,
                                child: CupertinoTextField(
                                  onChanged: (value) {
                                    setState(() {
                                      if(value.length >= 8) {
                                        immatriculationFieldSuffix = Row(
                                          children: [
                                            Container(
                                              width: 22,
                                              height: 22,
                                              decoration: BoxDecoration(
                                                  shape: BoxShape.circle,
                                                  border: Border.all(width: 2, color: primaryGreenColor)
                                              ),
                                              child: const Icon(Icons.check, color: primaryGreenColor, size: 15,),
                                            ),
                                            const SizedBox(width: 10,),
                                          ],
                                        );
                                      } else {
                                        immatriculationFieldSuffix = null;
                                      }
                                    });
                                  },
                                  controller: immatriculationFieldController,
                                  prefix:  const Row(
                                    children: [
                                      SizedBox(width: 10,),
                                      Icon(FontAwesomeIcons.car),
                                      SizedBox(width: 20,)
                                    ],
                                  ),
                                  suffix: immatriculationFieldSuffix,
                                  placeholder: 'Indiquez le numéro de plaque',
                                  keyboardType: TextInputType.text,
                                  decoration: BoxDecoration(
                                      color: textFieldBackground,
                                      borderRadius: BorderRadius.circular(15)
                                  ),
                                ),
                              ),
                              const SizedBox(height: 20,),
                              Row(
                                children: [
                                  boxToCheck(),
                                  const SizedBox(width: 20,),
                                  SizedBox(
                                    width: screenWidth * 0.7,
                                    child: RichText(
                                      text: const TextSpan(
                                          text: "En créant un compte, vous acceptez nos conditions d'utilissation.",
                                          style: TextStyle(
                                            fontFamily: primaryTextFont,
                                            color: Colors.grey,
                                          ),
                                          children: [
                                            TextSpan(
                                                text: " Conditiond'utilisation",
                                                style: TextStyle(
                                                    color: primaryGreenColor,
                                                    fontFamily: primaryTextFont
                                                )
                                            )
                                          ]
                                      ),
                                    ),
                                  )
                                ],
                              ),

                            ],
                          ),
                        ),
                        const Spacer(),
                        Container(
                          width: screenWidth * 0.82,
                          height: 50,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(15),
                              color: primaryGreenColor
                          ),
                          child: TextButton(
                            onPressed: () {
                              bool err = false;
                              String errorCause = '';
                              if(permisNumFieldSuffix == null) {
                                errorCause = 'votre numéro de permis';
                                err = true;
                              } else if(vehicle == 'Marque de votre véhicule') {
                                errorCause = 'votre marque de véhicule';
                                err = true;
                              } else if(immatriculationFieldSuffix == null) {
                                errorCause = "Votre nuéro de plaque d'immatriculation";
                                err = true;
                              }

                              if(err) {
                                ScaffoldMessenger.of(context).showSnackBar(
                                    SnackBar(
                                      content: Text('Veuillez bien renseigner $errorCause',
                                        style: const TextStyle(color: Colors.white),),
                                      backgroundColor: Colors.red,
                                    )
                                );
                              } else {
                                Navigator.push(context, SlideRightPageRoute(page: const LoginScreen()));
                              }

                            },
                            child: const Text(
                              "TERMINER L'INSCRIPTION",
                              style: TextStyle(
                                  letterSpacing: 0.5,
                                  fontSize: 16,
                                  fontFamily: primaryTextFont,
                                  color: Colors.white
                              ),
                            ),
                          ),
                        ),
                        const SizedBox(height: 50,)
                      ],
                    ),
                  ),
                )
              ],
            ),
            Positioned(
              width: 80,
              height: 80,
              left: screenWidth * 0.39,
              top: screenHeight * 0.074,
              child: const CircleAvatar(
                backgroundColor: primaryCircleAvatarColor,
                child: Icon(FontAwesomeIcons.car, size: 50,),
              ),

            ),
          ],
        ),
      ),
    );
  }


  // --custom comment-- checkbox widget
  Widget boxToCheck() {
    return Container(
      width: 25,
      height: 25,
      decoration: BoxDecoration(
        border: Border.all(width: 2, color: primaryGreenColor),
        color: Colors.white,
      ),
      child: const Icon(Icons.check, color: primaryGreenColor, size: 20,)
    );
  }

}