// --custom comment-- mean comment written by developer
// because as a developer, i use intelij which automatically comment line

import 'package:contravention/pages/login/login_screen.dart';
import 'package:contravention/pages/register/second_step.dart';
import 'package:contravention/reUsable/slide_left_to_right.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:contravention/reUsable/constants_variables.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';


class RegisterFirstStep extends StatefulWidget {
  const RegisterFirstStep({super.key});

  @override
  State<RegisterFirstStep> createState() => _RegisterFirstStepState();
}

class _RegisterFirstStepState extends State<RegisterFirstStep> {

  late TextEditingController nameTextFieldController;
  late TextEditingController phoneTextFieldController;
  late TextEditingController pwdTextFieldController;
  Widget? nameFieldSuffix;
  Widget? phoneFieldSuffix;
  Widget? pwdFieldSuffix;
  bool isChecked = false;

  @override
  void initState() {
    nameTextFieldController = TextEditingController();
    phoneTextFieldController = TextEditingController();
    pwdTextFieldController = TextEditingController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: primaryBackground,
        leading: const Icon(FontAwesomeIcons.xmark, color: Colors.transparent,),
      ),
      backgroundColor: primaryBackground,
      body: SingleChildScrollView(
        // --Custom comment-- we use Stack here because we want
        // to freely move the avatar
        child: Stack(
          children: [
            Column(
              children: [
                Center(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      IconButton(
                          onPressed: () {Navigator.pop(context);},
                          icon: const Icon(FontAwesomeIcons.xmark, size: 16,)),
                      SizedBox(width: screenWidth * 0.1,),
                      InkWell(
                        onTap: () {
                          Navigator.push(context, SlideRightPageRoute(page: const LoginScreen()));
                        },
                        child: RichText(
                          text: const TextSpan(
                              text: 'Vous avez déjà un compte ? ',
                              style: TextStyle(
                                  color: textColor, fontSize: 14, fontFamily: primaryTextFont),
                              children: <TextSpan>[
                                TextSpan(text:'Connexion', style: TextStyle(
                                    fontWeight: FontWeight.w600, color: primaryGreenColor,
                                    fontSize: 14, fontFamily: primaryTextFont),
                                )
                              ]
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                SizedBox(height: screenHeight * 0.06,),
                Container(
                  width: screenWidth,
                  height: screenHeight ,
                  decoration: const BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(40),
                          topRight: Radius.circular(40)
                      )
                  ),
                  child: Center(
                    child: Column(
                      children: [
                        const SizedBox(height: 60,),
                        const Text(
                          'Inscription',
                          style: TextStyle(
                              fontWeight: FontWeight.w600,
                              fontFamily: primaryTextFont, color: textColor,
                              fontSize: 24,
                              letterSpacing: 0.5
                        ),),
                        const SizedBox(height: 15,),
                        const Text(
                          'Créez votre compte et continuez',
                          style: TextStyle(
                            fontWeight: FontWeight.w400,
                            color: greyColor
                          ),
                        ),
                        const SizedBox(height: 45,),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: screenWidth * 0.06),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const Text(
                                'Votre nom',
                                style: TextStyle(
                                  fontWeight: FontWeight.w500,
                                  color: greyColor,
                                  fontSize: 14
                                ),
                              ),
                              const SizedBox(height: 10,),
                              SizedBox(
                                height: 55,
                                child: CupertinoTextField(
                                  onChanged: (value) {
                                    setState(() {
                                      if(value.length >= 2) {
                                        nameFieldSuffix = Row(
                                          children: [
                                            Container(
                                              width: 22,
                                              height: 22,
                                              decoration: BoxDecoration(
                                                  shape: BoxShape.circle,
                                                  border: Border.all(width: 2, color: primaryGreenColor)
                                              ),
                                              child: const Icon(Icons.check, color: primaryGreenColor, size: 15,),
                                            ),
                                            const SizedBox(width: 10,),
                                          ],
                                        );
                                      } else {
                                       nameFieldSuffix = null;
                                      }
                                    });
                                  },
                                  controller: nameTextFieldController,
                                  prefix:  const Row(
                                    children: [
                                      SizedBox(width: 10,),
                                      Icon(FontAwesomeIcons.circleUser),
                                      SizedBox(width: 20,)
                                    ],
                                  ),
                                  suffix: nameFieldSuffix,
                                  placeholder: 'Entrez votre nom ici',
                                  keyboardType: TextInputType.text,
                                  decoration: BoxDecoration(
                                    color: textFieldBackground,
                                    borderRadius: BorderRadius.circular(15)
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                        SizedBox(height: screenHeight * 0.03,),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: screenWidth * 0.06),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const Text(
                                'Numéro de téléphone',
                                style: TextStyle(
                                    fontWeight: FontWeight.w500,
                                    color: greyColor,
                                    fontSize: 14
                                ),
                              ),
                              const SizedBox(height: 10,),
                              SizedBox(
                                height: 55,
                                child: CupertinoTextField(
                                  onChanged: (value) {
                                    setState(() {
                                      if(value.length >= 2 && RegExp(r'^(01|05|07)[0-9]{8}$').hasMatch(value)) {
                                        phoneFieldSuffix = Row(
                                          children: [
                                            Container(
                                              width: 22,
                                              height: 22,
                                              decoration: BoxDecoration(
                                                  shape: BoxShape.circle,
                                                  border: Border.all(width: 2, color: primaryGreenColor)
                                              ),
                                              child: const Icon(Icons.check, color: primaryGreenColor, size: 15,),
                                            ),
                                            const SizedBox(width: 10,),
                                          ],
                                        );
                                      } else {
                                       phoneFieldSuffix = null;
                                      }
                                    });
                                  },
                                  controller: phoneTextFieldController,
                                  prefix:  const Row(
                                    children: [
                                      SizedBox(width: 10,),
                                      Icon(Icons.phone_android_outlined),
                                      SizedBox(width: 10,),
                                      Text('(+225)', style: TextStyle(color: textColor,
                                          fontFamily: primaryTextFont, fontSize: 14),),
                                      SizedBox(width: 2,)
                                    ],
                                  ),
                                  suffix: phoneFieldSuffix,
                                  placeholder: 'Entrez votre numéro de téléphone ici',
                                  keyboardType: TextInputType.phone,
                                  decoration: BoxDecoration(
                                      color: textFieldBackground,
                                      borderRadius: BorderRadius.circular(15)
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                        SizedBox(height: screenHeight * 0.03,),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: screenWidth * 0.06),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const Text(
                                'Mot de passe',
                                style: TextStyle(
                                    fontWeight: FontWeight.w500,
                                    color: greyColor,
                                    fontSize: 14
                                ),
                              ),
                              const SizedBox(height: 10,),
                              SizedBox(
                                height: 55,
                                child: CupertinoTextField(
                                  obscureText: true,
                                  onChanged: (value) {
                                    setState(() {
                                      if(value.length >= 8) {
                                        pwdFieldSuffix = Row(
                                          children: [
                                            Container(
                                              width: 22,
                                              height: 22,
                                              decoration: BoxDecoration(
                                                  shape: BoxShape.circle,
                                                  border: Border.all(width: 2, color: primaryGreenColor)
                                              ),
                                              child: const Icon(Icons.check, color: primaryGreenColor, size: 15,),
                                            ),
                                            const SizedBox(width: 10,),
                                          ],
                                        );
                                      } else {
                                        pwdFieldSuffix = null;
                                      }
                                    });
                                  },
                                  controller: pwdTextFieldController,
                                  prefix:  const Row(
                                    children: [
                                      SizedBox(width: 10,),
                                      Icon(Icons.lock_outline_rounded),
                                      SizedBox(width: 20,)
                                    ],
                                  ),
                                  suffix: pwdFieldSuffix,
                                  placeholder: 'Indiquez un mot de passe',
                                  keyboardType: TextInputType.text,
                                  decoration: BoxDecoration(
                                      color: textFieldBackground,
                                      borderRadius: BorderRadius.circular(15)
                                  ),
                                ),
                              ),
                              const SizedBox(height: 20,),
                              Row(
                                children: [
                                  boxToCheck(),
                                  const SizedBox(width: 20,),
                                  SizedBox(
                                    width: screenWidth * 0.7,
                                    child: RichText(
                                      text: const TextSpan(
                                        text: "En créant un compte, vous acceptez nos conditions d'utilissation.",
                                        style: TextStyle(
                                          fontFamily: primaryTextFont,
                                          color: Colors.grey,
                                        ),
                                        children: [
                                          TextSpan(
                                            text: " Conditiond'utilisation",
                                            style: TextStyle(
                                              color: primaryGreenColor,
                                              fontFamily: primaryTextFont
                                            )
                                          )
                                        ]
                                      ),
                                    ),
                                  )
                                ],
                              ),

                            ],
                          ),
                        ),
                        const Spacer(),
                        Container(
                          width: screenWidth * 0.82,
                          height: 50,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(15),
                            color: primaryGreenColor
                          ),
                          child: TextButton(
                            onPressed: () {
                              bool err = false;
                              String errorCause = '';
                              if(nameFieldSuffix == null) {
                                errorCause = 'Nom';
                                err = true;
                              } else if(phoneFieldSuffix == null) {
                                errorCause = 'Téléphone';
                                err = true;
                              } else if(pwdFieldSuffix == null) {
                                errorCause = 'Mot de passe';
                                err = true;
                              }

                              if(err) {
                                ScaffoldMessenger.of(context).showSnackBar(
                                    SnackBar(
                                      content: Text('Veuillez bien renseigner le champs $errorCause',
                                        style: const TextStyle(color: Colors.white),),
                                      backgroundColor: Colors.red,
                                    )
                                );
                              } else if(!isChecked) {
                                ScaffoldMessenger.of(context).showSnackBar(
                                    const SnackBar(
                                      content: Text("Veuillez accepter nos conditions d'utilisation avant de continuer",
                                        style: TextStyle(color: Colors.white),),
                                      backgroundColor: Colors.red,
                                    )
                                );
                              } else {
                                Navigator.push(context, SlideRightPageRoute(page: const RegisterSecondStep()));
                              }

                            },
                            child: const Text(
                              'CONTINUER',
                              style: TextStyle(
                                  letterSpacing: 0.5,
                                  fontSize: 16,
                                  fontFamily: primaryTextFont,
                                  color: Colors.white
                              ),
                            ),
                          ),
                        ),
                        const SizedBox(height: 50,)
                      ],
                    ),
                  ),
                )
              ],
            ),
            Positioned(
              width: 80,
              height: 80,
              left: screenWidth * 0.39,
              top: screenHeight * 0.074,
              child: const CircleAvatar(
                backgroundColor: primaryCircleAvatarColor,
                child: Icon(FontAwesomeIcons.user, size: 50,),
              ),

            ),
          ],
        ),
      ),
    );
  }


  // --custom comment-- checkbox widget
  Widget boxToCheck() {
    Color getColor(Set<MaterialState> states) {
      return Colors.transparent;
    }
    return Container(
      width: 20,
      height: 20,
      decoration: BoxDecoration(
        border: Border.all(width: 2, color: primaryGreenColor),
      ),
      child: Checkbox(
        side: BorderSide.none,
        checkColor: primaryGreenColor,
        fillColor: MaterialStateProperty.resolveWith(getColor),
        value: isChecked,
        onChanged: (bool? value) {
          setState(() {
            isChecked = value!;
          });
        },
      ),
    );
  }

}