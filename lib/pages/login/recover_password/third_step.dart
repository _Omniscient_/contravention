// --custom comment-- mean comment written by developer
// because as a developer, i use intelij which automatically comment line

import 'package:contravention/pages/home/dashboard.dart';
import 'package:contravention/reUsable/slide_left_to_right.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:contravention/reUsable/constants_variables.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';


class NewPwd extends StatefulWidget {
  const NewPwd({super.key});

  @override
  State<NewPwd> createState() => _NewPwdState();
}

class _NewPwdState extends State<NewPwd> {

  late TextEditingController firstTimePwdController;
  late TextEditingController secondTimePwdController;


  @override
  void initState() {
    firstTimePwdController = TextEditingController();
    secondTimePwdController = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    firstTimePwdController.dispose();
    secondTimePwdController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: primaryBackground,
        leading: const Icon(FontAwesomeIcons.xmark, color: Colors.transparent,),
      ),
      backgroundColor: primaryBackground,
      body: SingleChildScrollView(
        // --Custom comment-- we use Stack here because we want
        // to freely move the avatar
        child: Stack(
          children: [
            Column(
              children: [
                SizedBox(height: screenHeight * 0.12,),
                Container(
                  width: screenWidth,
                  height: screenHeight * 0.8,
                  decoration: const BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(40),
                          topRight: Radius.circular(40)
                      )
                  ),
                  child: Center(
                    child: Column(
                      children: [
                        const SizedBox(height: 90,),
                        const Text(
                          'Modifiez votre mot de passe',
                          style: TextStyle(
                              fontWeight: FontWeight.w600,
                              fontFamily: primaryTextFont, color: textColor,
                              fontSize: 24,
                              letterSpacing: 0.5
                          ),),
                        const SizedBox(height: 15,),
                        const Text(
                          'Au moins 8 caractères, avec des majuscules',
                          style: TextStyle(
                              fontWeight: FontWeight.w400,
                              color: textColor
                          ),
                        ),
                        const SizedBox(height: 5,),
                        const Text(
                          'et des minuscules',
                          style: TextStyle(
                              fontWeight: FontWeight.w400,
                              color: textColor
                          ),
                        ),
                        const SizedBox(height: 45,),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: screenWidth * 0.06),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const Text(
                                'Nouveau mot de passe',
                                style: TextStyle(
                                    fontWeight: FontWeight.w500,
                                    color: greyColor,
                                    fontSize: 14
                                ),
                              ),
                              const SizedBox(height: 10,),
                              SizedBox(
                                height: 55,
                                child: CupertinoTextField(
                                  placeholder: 'nouveau mot de passe',
                                  obscureText: true,
                                  controller: firstTimePwdController,
                                  prefix:  const Row(
                                    children: [
                                      SizedBox(width: 10,),
                                      Icon(Icons.lock),
                                      SizedBox(width: 5,)
                                    ],
                                  ),

                                  keyboardType: TextInputType.text,
                                  decoration: BoxDecoration(
                                      color: textFieldBackground,
                                      borderRadius: BorderRadius.circular(15)
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                        SizedBox(height: screenHeight * 0.03,),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: screenWidth * 0.06),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const Text(
                                'Confirmer le mot de passe',
                                style: TextStyle(
                                    fontWeight: FontWeight.w500,
                                    color: greyColor,
                                    fontSize: 14
                                ),
                              ),
                              const SizedBox(height: 10,),
                              SizedBox(
                                height: 55,
                                child: CupertinoTextField(
                                  placeholder: 'confirmer le mot de passe',
                                  obscureText: true,
                                  controller: secondTimePwdController,
                                  prefix:  const Row(
                                    children: [
                                      SizedBox(width: 10,),
                                      Icon(Icons.lock),
                                      SizedBox(width: 20,)
                                    ],
                                  ),
                                  keyboardType: TextInputType.text,
                                  decoration: BoxDecoration(
                                      color: textFieldBackground,
                                      borderRadius: BorderRadius.circular(15)
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        const Spacer(),
                        Container(
                          width: screenWidth * 0.82,
                          height: 50,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(15),
                              color: primaryGreenColor
                          ),
                          child: TextButton(
                            onPressed: () {
                              bool err = false;
                              String errTxt = '';
                              if (firstTimePwdController.text.isEmpty
                                  || secondTimePwdController.text.isEmpty) {

                                err = true;
                                errTxt = 'Veuillez bien remplir les champs';

                              } else if(firstTimePwdController.text != secondTimePwdController.text) {
                                err = true;
                                errTxt = 'Les mots de passe ne correspondent pas';
                              } else if(firstTimePwdController.text.length < 8) {
                                err = true;
                                errTxt = 'Veuillez indiquer au moins 8 caracteres';
                              }

                              if (err) {
                                ScaffoldMessenger.of(context).showSnackBar(
                                    SnackBar(
                                      content: Text(
                                        errTxt,
                                        style: const TextStyle(
                                            color: Colors.white),),
                                      backgroundColor: Colors.red,
                                    )
                                );
                              } else {
                                Navigator.push(context, SlideRightPageRoute(
                                    page: const Dashboard()));
                              }
                            },
                            child: const Text(
                              'CONNEXION',
                              style: TextStyle(
                                  letterSpacing: 0.5,
                                  fontSize: 16,
                                  fontFamily: primaryTextFont,
                                  color: Colors.white
                              ),
                            ),
                          ),
                        ),
                        const SizedBox(height: 40,)
                      ],
                    ),
                  ),
                )
              ],
            ),
            Positioned(
              width: 80,
              height: 80,
              left: screenWidth * 0.39,
              top: screenHeight * 0.074,
              child: const CircleAvatar(
                backgroundColor: primaryCircleAvatarColor,
                child: Icon(FontAwesomeIcons.lock, size: 50,),
              ),

            ),
          ],
        ),
      ),
    );
  }


}