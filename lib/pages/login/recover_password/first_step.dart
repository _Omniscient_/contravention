// --custom comment-- mean comment written by developer
// because as a developer, i use intelij which automatically comment line

import 'package:contravention/pages/login/recover_password/second_sted.dart';
import 'package:contravention/reUsable/slide_left_to_right.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:contravention/reUsable/constants_variables.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';


class RecoverPwdFirstStep extends StatefulWidget {
  const RecoverPwdFirstStep({super.key});

  @override
  State<RecoverPwdFirstStep> createState() => _RecoverPwdFirstStepState();
}

class _RecoverPwdFirstStepState extends State<RecoverPwdFirstStep> {

  late TextEditingController phoneTextFieldController;
  late TextEditingController pwdTextFieldController;

  Widget? nameFieldSuffix;
  Widget? phoneFieldSuffix;

  @override
  void initState() {
    phoneTextFieldController = TextEditingController();
    pwdTextFieldController = TextEditingController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: primaryBackground,
        leading: const Icon(FontAwesomeIcons.xmark, color: Colors.transparent,),
      ),
      backgroundColor: primaryBackground,
      body: SingleChildScrollView(
        // --Custom comment-- we use Stack here because we want
        // to freely move the avatar
        child: Stack(
          children: [
            Column(
              children: [
                Center(
                  child: Row(
                    children: [
                      IconButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        icon: const Icon(FontAwesomeIcons.xmark, size: 16,),
                      ),

                    ],
                  ),
                ),
                SizedBox(height: screenHeight * 0.068,),
                Container(
                  width: screenWidth,
                  height: screenHeight * 0.8,
                  decoration: const BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(40),
                          topRight: Radius.circular(40)
                      )
                  ),
                  child: Center(
                    child: Column(
                      children: [
                        const SizedBox(height: 90,),
                        const Text(
                          'Réinitialiser mot de passe',
                          style: TextStyle(
                              fontWeight: FontWeight.w600,
                              fontFamily: primaryTextFont, color: textColor,
                              fontSize: 24,
                              letterSpacing: 0.5
                          ),),
                        const SizedBox(height: 15,),
                        const Text(
                          'Entrez votre numéro de téléphone pour recevoir',
                          style: TextStyle(
                              fontWeight: FontWeight.w400,
                              color: greyColor
                          ),
                        ),
                        const SizedBox(height: 5,),
                        const Text(
                          ' le code de réinitialisations',
                          style: TextStyle(
                            color: greyColor,
                            fontSize: 14,
                            fontWeight: FontWeight.w400,
                            fontFamily: primaryTextFont,
                          ),
                        ),
                        const SizedBox(height: 60,),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: screenWidth * 0.06),
                          child: SizedBox(
                            height: 85,
                            child: CupertinoTextField(
                              onChanged: (value) {
                                setState(() {
                                  if(value.length >= 2 && RegExp(r'^(01|05|07)[0-9]{8}$').hasMatch(value)) {
                                    phoneFieldSuffix = Row(
                                      children: [
                                        Container(
                                          width: 22,
                                          height: 22,
                                          decoration: BoxDecoration(
                                              shape: BoxShape.circle,
                                              border: Border.all(width: 2, color: primaryGreenColor)
                                          ),
                                          child: const Icon(Icons.check, color: primaryGreenColor, size: 15,),
                                        ),
                                        const SizedBox(width: 10,),
                                      ],
                                    );
                                  } else {
                                    phoneFieldSuffix = null;
                                  }
                                });
                              },
                              controller: phoneTextFieldController,
                              prefix: Column(
                                children: [
                                  const SizedBox(height: 7,),
                                  const Row(
                                    children: [
                                      SizedBox(width: 10,),
                                      Text('Numéro de téléphone',
                                        style: TextStyle(
                                          fontFamily: primaryTextFont,
                                          color: greyColor,
                                        ),
                                      ),
                                    ],
                                  ),
                                  const SizedBox(height: 1,),
                                  Row(
                                    children: [
                                      // const SizedBox(width: 10,),
                                      Container(
                                        width: 40,
                                      height: 40,
                                        decoration: const BoxDecoration(
                                          shape: BoxShape.circle,
                                          image: DecorationImage(
                                            image: AssetImage('assets/imgs/ivoryCoast_flag.png')
                                          )
                                        ),
                                      ),
                                      const SizedBox(width: 10,),
                                      const Text('(+225)', style: TextStyle(color: textColor,
                                          fontFamily: primaryTextFont, fontSize: 14),),
                                      const SizedBox(width: 20,)
                                    ],
                                  ),
                                ],
                              ),
                              suffix: phoneFieldSuffix,
                              placeholder: 'numéro de téléphone',
                              keyboardType: TextInputType.phone,
                              decoration: BoxDecoration(
                                  color: textFieldBackground,
                                  borderRadius: BorderRadius.circular(15)
                              ),
                            ),
                          ),
                        ),

                        const Spacer(),
                        Container(
                          width: screenWidth * 0.82,
                          height: 50,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(15),
                              color: primaryGreenColor
                          ),
                          child: TextButton(
                            onPressed: () {
                              bool err = false;
                              String errorCause = '';
                              if (phoneFieldSuffix == null) {
                                err = true;
                                errorCause = 'Téléphone';
                              }

                              if (err) {
                                ScaffoldMessenger.of(context).showSnackBar(
                                    SnackBar(
                                      content: Text(
                                        'Veuillez bien renseigner le champs $errorCause',
                                        style: const TextStyle(
                                            color: Colors.white),),
                                      backgroundColor: Colors.red,
                                    )
                                );
                              } else {
                                Navigator.push(context, SlideRightPageRoute(
                                    page: const RecoverPwdSecondStep(
                                      number: '(+225) 0759302928',)));
                              }
                            },
                            child: const Text(
                              'ENVOYER LE CODE',
                              style: TextStyle(
                                  letterSpacing: 0.5,
                                  fontSize: 16,
                                  fontFamily: primaryTextFont,
                                  color: Colors.white
                              ),
                            ),
                          ),
                        ),

                        const SizedBox(height: 50,),
                      ],
                    ),
                  ),
                )
              ],
            ),
            Positioned(
              width: 80,
              height: 80,
              left: screenWidth * 0.39,
              top: screenHeight * 0.074,
              child: const CircleAvatar(
                backgroundColor: primaryCircleAvatarColor,
                child: Icon(Icons.lock_open_outlined, size: 50,),
              ),

            ),
          ],
        ),
      ),
    );
  }


}