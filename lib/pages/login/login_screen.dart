// --custom comment-- mean comment written by developer
// because as a developer, i use intelij which automatically comment line

import 'package:contravention/pages/login/confirmation.dart';
import 'package:contravention/pages/login/recover_password/first_step.dart';
import 'package:contravention/pages/register/first_step.dart';
import 'package:contravention/reUsable/slide_left_to_right.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:contravention/reUsable/constants_variables.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';


class LoginScreen extends StatefulWidget {
  const LoginScreen({super.key});

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {

  late TextEditingController phoneTextFieldController;
  late TextEditingController pwdTextFieldController;

  Widget? nameFieldSuffix;
  Widget? phoneFieldSuffix;

  @override
  void initState() {
    phoneTextFieldController = TextEditingController();
    pwdTextFieldController = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    phoneTextFieldController.dispose();
    pwdTextFieldController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: primaryBackground,
        leading: const Icon(FontAwesomeIcons.xmark, color: Colors.transparent,),
      ),
      backgroundColor: primaryBackground,
      body: SingleChildScrollView(
        // --Custom comment-- we use Stack here because we want
        // to freely move the avatar
        child: Stack(
          children: [
            Column(
              children: [
                Center(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      IconButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        icon: const Icon(FontAwesomeIcons.xmark, size: 16,),
                      ),
                      SizedBox(width: screenWidth * 0.4,),
                      TextButton(
                        onPressed: () {Navigator.push(context, SlideRightPageRoute(page: const RecoverPwdFirstStep()));},
                        child: const Text('Mot de passe oublié ?', style: TextStyle(
                          fontFamily: primaryTextFont,
                          color: textColor
                        ),),
                      )
                    ],
                  ),
                ),
                SizedBox(height: screenHeight * 0.068,),
                Container(
                  width: screenWidth,
                  height: screenHeight * 0.83,
                  decoration: const BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(40),
                          topRight: Radius.circular(40)
                      )
                  ),
                  child: Center(
                    child: Column(
                      children: [
                        const SizedBox(height: 60,),
                        const Text(
                          'Connexion',
                          style: TextStyle(
                              fontWeight: FontWeight.w600,
                              fontFamily: primaryTextFont, color: textColor,
                              fontSize: 24,
                              letterSpacing: 0.5
                          ),),
                        const SizedBox(height: 15,),
                        const Text(
                          'Veuillez renseignez vos informations',
                          style: TextStyle(
                              fontWeight: FontWeight.w400,
                              color: primaryGreenColor
                          ),
                        ),
                        const SizedBox(height: 45,),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: screenWidth * 0.06),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const Text(
                                'Numéro de téléphone',
                                style: TextStyle(
                                    fontWeight: FontWeight.w500,
                                    color: greyColor,
                                    fontSize: 14
                                ),
                              ),
                              const SizedBox(height: 10,),
                              SizedBox(
                                height: 55,
                                child: CupertinoTextField(
                                  onChanged: (value) {
                                    setState(() {
                                      if(value.length >= 2 && RegExp(r'^(01|05|07)[0-9]{8}$').hasMatch(value)) {
                                        phoneFieldSuffix = Row(
                                          children: [
                                            Container(
                                              width: 22,
                                              height: 22,
                                              decoration: BoxDecoration(
                                                  shape: BoxShape.circle,
                                                  border: Border.all(width: 2, color: primaryGreenColor)
                                              ),
                                              child: const Icon(Icons.check, color: primaryGreenColor, size: 15,),
                                            ),
                                            const SizedBox(width: 10,),
                                          ],
                                        );
                                      } else {
                                        phoneFieldSuffix = null;
                                      }
                                    });
                                  },
                                  controller: phoneTextFieldController,
                                  prefix:  const Row(
                                    children: [
                                      SizedBox(width: 10,),
                                      Icon(Icons.phone_android_outlined),
                                      SizedBox(width: 10,),
                                      Text('(+225)', style: TextStyle(color: textColor,
                                          fontFamily: primaryTextFont, fontSize: 14),),
                                      SizedBox(width: 5,)
                                    ],
                                  ),
                                  suffix: phoneFieldSuffix,
                                  placeholder: 'numéro de téléphone',
                                  keyboardType: TextInputType.phone,
                                  decoration: BoxDecoration(
                                      color: textFieldBackground,
                                      borderRadius: BorderRadius.circular(15)
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                        SizedBox(height: screenHeight * 0.03,),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: screenWidth * 0.06),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const Text(
                                'Mot de passe',
                                style: TextStyle(
                                    fontWeight: FontWeight.w500,
                                    color: greyColor,
                                    fontSize: 14
                                ),
                              ),
                              const SizedBox(height: 10,),
                              SizedBox(
                                height: 55,
                                child: CupertinoTextField(
                                  obscureText: true,
                                  controller: pwdTextFieldController,
                                  prefix:  const Row(
                                    children: [
                                      SizedBox(width: 10,),
                                      Icon(Icons.lock_outline_rounded),
                                      SizedBox(width: 20,)
                                    ],
                                  ),
                                  placeholder: 'Indiquez votre mot de passe',
                                  keyboardType: TextInputType.text,
                                  decoration: BoxDecoration(
                                      color: textFieldBackground,
                                      borderRadius: BorderRadius.circular(15)
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        const Spacer(),
                        Container(
                          width: screenWidth * 0.82,
                          height: 50,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(15),
                              color: primaryGreenColor
                          ),
                          child: TextButton(
                            onPressed: () {
                              bool err = false;
                              String errorCause = '';
                              if (phoneFieldSuffix == null) {
                                err = true;
                                errorCause = 'Téléphone';

                              } else if(pwdTextFieldController.text.isEmpty) {
                                err = true;
                                errorCause = 'Mot de passe';
                              }

                              if (err) {
                                ScaffoldMessenger.of(context).showSnackBar(
                                    SnackBar(
                                      content: Text(
                                        'Veuillez bien renseigner le champs $errorCause',
                                        style: const TextStyle(
                                            color: Colors.white),),
                                      backgroundColor: Colors.red,
                                    )
                                );
                              } else {
                                Navigator.push(context, SlideRightPageRoute(
                                    page: const Confirmation(
                                      number: '(+225) 0759302928',)));
                              }
                            },
                            child: const Text(
                              'CONNEXION',
                              style: TextStyle(
                                  letterSpacing: 0.5,
                                  fontSize: 16,
                                  fontFamily: primaryTextFont,
                                  color: Colors.white
                              ),
                            ),
                          ),
                        ),
                        TextButton(
                          onPressed: () {Navigator.push(context, SlideRightPageRoute(page: const RegisterFirstStep()));},
                          child: const Text(
                            'Créer un compte',
                            style: TextStyle(
                              fontFamily: primaryTextFont,
                              color: textColor,
                            ),
                          ),
                        ),
                        const SizedBox(height: 50,),
                      ],
                    ),
                  ),
                )
              ],
            ),
            Positioned(
              width: 80,
              height: 80,
              left: screenWidth * 0.39,
              top: screenHeight * 0.074,
              child: const CircleAvatar(
                backgroundColor: primaryCircleAvatarColor,
                child: Icon(FontAwesomeIcons.user, size: 50,),
              ),

            ),
          ],
        ),
      ),
    );
  }


}