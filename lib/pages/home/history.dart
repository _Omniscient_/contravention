
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../../reUsable/constants_variables.dart';
import '../../reUsable/history_model.dart';


class HistoryHomeScreen extends StatefulWidget {
  const HistoryHomeScreen({super.key});

  @override
  State<HistoryHomeScreen> createState() => _HistoryHomeScreenState();
}

class _HistoryHomeScreenState extends State<HistoryHomeScreen> {
  late List<HistoryModel> myHistoryList;
  late List<HistoryModel> pseudoCopyOfHistory;


  @override
  void initState() {
    HistoryModel.addToAllListForTestPurpose();

    setState(() {
      myHistoryList = HistoryModel.allHistoryList;
       pseudoCopyOfHistory = [...HistoryModel.allHistoryList];
    });
    super.initState();
  }

  @override
  void dispose() {
    HistoryModel.allHistoryList = [];
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).scaffoldBackgroundColor,
        elevation: 0,
        leading: const Icon(FontAwesomeIcons.xmark, color: Colors.transparent,),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Column(
            children: [
              Row(
                children: [
                  const Text(
                    'History',
                    style: TextStyle(
                      fontSize: 30,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  const Spacer(),
                  IconButton (
                    onPressed: () {  },
                    icon: const Icon(Icons.tune, color: Colors.blue,),
                  )
                ],
              ),
              const SizedBox(height: 30,),
              // ignore: unused_local_variable
              for(var item in [...pseudoCopyOfHistory])
                historyCard()
            ],
          ),
        ),
      ),
    );
  }

  Widget historyCard() {

    if(pseudoCopyOfHistory.isEmpty) {
      return const SizedBox();
    } else {
      String referenceDate = pseudoCopyOfHistory[0].date;
      List<HistoryModel> historyCopy = [];


      for(HistoryModel element in [...pseudoCopyOfHistory]) {
        if(element.date == referenceDate) {
          historyCopy.add(element);
          pseudoCopyOfHistory.remove(element);
        }
      }

      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            referenceDate,
            style: const TextStyle(
                fontWeight: FontWeight.w600),
          ),
          const SizedBox(height: 20,),
          Container(
            decoration: BoxDecoration(
                color: Colors.blue.withOpacity(0.07),
                borderRadius: BorderRadius.circular(20)
            ),
            child: Column(
              children: List.generate(
                  historyCopy.length,
                      (i) {
                    return InkWell(
                      onTap: () {},
                      child: ListTile(
                        leading: historyCopy[i].type == 'add' ?
                        CircleAvatar(
                          backgroundColor: Colors.white,
                          child: Image.asset(historyCopy[i].logo),
                        ) : historyCopy[i].type == 'minus' ?
                        Container(
                          width: 30,
                          height: 30,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              border: Border.all(width:2, color: primaryGreenColor),
                              borderRadius: BorderRadius.circular(50)
                          ),
                          child: const Icon(Icons.check),
                        ) : null,
                        title: Text(
                            historyCopy[i].title,
                            style: const TextStyle(
                              fontFamily: primaryTextFont,
                              color: textColor,
                              fontSize: 14,
                            )
                        ),
                        subtitle: Text(
                          historyCopy[i].detail,
                          style: const TextStyle(
                            fontFamily: primaryTextFont,
                            color: textColor,
                          ),
                        ),
                        trailing: Text(
                            historyCopy[i].price,
                            style: TextStyle(
                              fontFamily: primaryTextFont,
                              color: historyCopy[i].type == 'add' ?
                              primaryGreenColor : historyCopy[i].type == 'minus' ?
                              Colors.red : null,
                            )
                        ),
                      ),
                    );
                  }
              ),
            ),
          ),
          const SizedBox(height: 20,),
        ],
      );

    }
    }
}