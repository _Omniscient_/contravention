import 'package:contravention/pages/home/dashboard.dart';
import 'package:contravention/reUsable/constants_variables.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../../reUsable/slide_left_to_right.dart';

class PaymentSuccess extends StatelessWidget {
  const PaymentSuccess({super.key, required this.type});

  final String type;

  @override
  Widget build(BuildContext context) {
    String txt = type == 'checkout' ?
                'Paiement' : type == 'payment' ?
                'Rechargement' : '';
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: primaryBackground,
        leading: const Icon(FontAwesomeIcons.xmark, color: Colors.transparent,),
      ),
      backgroundColor: primaryBackground,
      body: Center(
        child: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(height: screenHeight * 0.05,),
             Stack(
               children: [
                 Container(
                   width: 250,
                   height: 250,
                   decoration: BoxDecoration(
                       shape: BoxShape.circle,
                       color: const Color(0xFF45cd82).withOpacity(0.19)
                   ),

                 ),
                 Positioned(
                   left: 24,
                   top: 25,
                   child: Container(
                     width: 200,
                     height: 200,
                     decoration: BoxDecoration(
                         shape: BoxShape.circle,
                         color: const Color(0xFF45cd82).withOpacity(0.19)
                     ),

                   ),
                 ),
                 Positioned(
                   left: 49,
                   top: 50,
                   child: Container(
                     width: 150,
                     height: 150,
                     decoration: const BoxDecoration(
                         shape: BoxShape.circle,
                         color: Color(0xFF45cd82)
                     ),
                     child: const Icon(Icons.check, color: Colors.white, size: 100,),
                   ),
                 ),
               ],
             ),
              const SizedBox(height: 30,),
              Text(
                '$txt effectué',
                style: const TextStyle(
                  fontSize: 30,
                  color: primaryGreenColor,

                ),
              ),
              const Text(
                "Votre compte a été débit avec succès.",
                  style: TextStyle(
                    fontFamily: primaryTextFont,
                    fontSize: 20,
                  )
              ),
              const SizedBox(height: 40,),
              Container(
                width: screenWidth * 0.82,
                height: 50,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15),
                    color: yellowColor
                ),
                child: TextButton.icon(
                  onPressed: () {

                  },
                  icon: const Icon(Icons.description, color: Colors.white,),
                  label: const Text(
                    'Télécharger le reçu',
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 20,
                        shadows: [
                          BoxShadow(
                              spreadRadius: 0.5,
                              blurRadius: 0.5,
                              color: Colors.black
                          )
                        ]
                    ),
                  ),
                ),
              ),
              const SizedBox(height: 30,),
              Padding(
                padding: EdgeInsets.only(right: screenWidth * 0.03),
                child: IconButton(
                  onPressed: () {Navigator.push(context, SlideRightPageRoute(page: const Dashboard()));},
                  icon: const Icon(Icons.home_outlined, size: 80, color: greyColor,),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }


}