
import 'package:contravention/pages/home/feed_your_account.dart';
import 'package:contravention/pages/home/history.dart';
import 'package:contravention/pages/home/qr.dart';
import 'package:contravention/reUsable/constants_variables.dart';
import 'package:contravention/reUsable/from_down_to_top.dart';
import 'package:contravention/reUsable/slide_left_to_right.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:contravention/reUsable/history_model.dart';



class Dashboard extends StatefulWidget {
  const Dashboard({super.key});

  @override
  State<Dashboard> createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  final String userName = "M'lan Alban";
  final String amount = "15 625";
  late List<HistoryModel> myHistoryList;

  @override
  void initState() {
    HistoryModel.addToRecentListForTestPurpose();

    setState(() {
      myHistoryList = HistoryModel.recentHistoryList;
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        leading: const  Icon(Icons.menu, color: textColor,),
        elevation: 0,
        backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      ),
      body: PopScope(
        canPop: false,
        child: SingleChildScrollView(
          child: Column(
            children: [
              Padding(
                padding: EdgeInsets.symmetric(horizontal: screenWidth * 0.1),
                child: Container(
                  height: 112,
                  decoration: BoxDecoration(
                    color: primaryGreenColor,
                    borderRadius: BorderRadius.circular(18)
                  ),
                  child: Center(
                    child: ListTile(
                      leading: const Icon(FontAwesomeIcons.user, size: 59, color: Colors.white,),
                      title: const Text(
                        'Bienvenue',
                        style: TextStyle(
                          fontSize: 14,
                            fontFamily: primaryTextFont,
                          color: Colors.white
                        ),
                      ),
                      subtitle: Text(
                        userName,
                        style: const TextStyle(
                          fontFamily: primaryTextFont,
                          fontSize: 30,
                          color: Colors.white
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              const SizedBox(height: 30,),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    '$amount FCFA',
                    style: const TextStyle(
                      fontSize: 40,
                      fontFamily: primaryTextFont,
                      color: textColor
                    ),
                  ),
                  const SizedBox(width: 60,),
                  Container(
                    width: 40,
                    height: 40,
                    decoration: const BoxDecoration(
                      color: primaryCircleAvatarColor,
                      shape: BoxShape.circle,
                    ),
                    child: IconButton(
                      onPressed: () {Navigator.push(context, SlideRightPageRoute(page: const FeedYourAccount()));},
                      icon: const Icon(FontAwesomeIcons.circlePlus, color: Colors.white,),
                    )
                  )
                ],
              ),
              Padding(
                padding: EdgeInsets.only(left: screenWidth * 0.06),
                child: const Row(
                  children: [
                    Text(
                      'votre solde',
                      style: TextStyle(
                        fontSize: 16,
                        fontFamily: primaryTextFont,
                      )
                    ),
                  ],
                ),
              ),
              SizedBox(height: screenHeight * 0.05,),
              Container(
                // height: screenHeight * 0.8,
                decoration: const BoxDecoration(
                  color: primaryBackground,
                  borderRadius: BorderRadius.only(
                    topRight: Radius.circular(40),
                    topLeft: Radius.circular(40)
                  )
                ),
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 30),
                      child: Container(
                        width: 30,
                        height: 5,
                        decoration: BoxDecoration(
                          color: greyColor,
                          borderRadius: BorderRadius.circular(15)
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const Text(
                            'VOS RECENTES TRANSACTIONS',
                              style: TextStyle(
                                fontSize: 16,
                                fontFamily: primaryTextFont,
                              )
                          ),

                          const Spacer(),
                          IconButton(
                            onPressed: () {
                              Navigator.push(context, FromDownToUp(page: const HistoryHomeScreen()));
                            },
                            icon: const Icon(Icons.history, size: 30,),

                          )
                        ],
                      ),
                    ),
                    for(int i = 0; i < 4; i++)
                      ListTile(
                        leading: myHistoryList[i].type == 'add' ?
                          CircleAvatar(
                            backgroundColor: Colors.white,
                            child: Image.asset(myHistoryList[i].logo),
                          ) : myHistoryList[i].type == 'minus' ?
                          Container(
                            width: 30,
                            height: 30,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              border: Border.all(width:2, color: primaryGreenColor),
                              borderRadius: BorderRadius.circular(50)
                            ),
                            child: const Icon(Icons.check),
                          ) : null,
                        title: Text(
                          myHistoryList[i].title,
                          style: const TextStyle(
                            fontFamily: primaryTextFont,
                            color: textColor,
                            fontSize: 14,
                          )
                        ),
                        subtitle: Text(
                          myHistoryList[i].detail,
                            style: const TextStyle(
                              fontFamily: primaryTextFont,
                              color: textColor,
                            ),
                        ),
                        trailing: Text(
                          myHistoryList[i].price,
                          style: TextStyle(
                            fontFamily: primaryTextFont,
                            color: myHistoryList[i].type == 'add' ?
                                  primaryGreenColor : myHistoryList[i].type == 'minus' ?
                                  Colors.red : null,
                          )
                        ),
                      ),
                    const SizedBox(height: 30,),
                    const Text(
                      'Scanner',
                      style: TextStyle(
                        fontFamily: primaryTextFont,
                        fontSize: 16,
                        fontWeight: FontWeight.w600
                      ),
                    ),
                    const SizedBox(height: 20,),
                    TextButton(
                      onPressed: () {
                        Navigator.push(context, SlideRightPageRoute(page:  const QRViewExample()));
                      },
                      child: Container(
                        width: 55,
                        height: 55,
                        decoration: const BoxDecoration(
                          color: primaryGreenColor,
                          shape: BoxShape.circle,
                        ),
                      )
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}