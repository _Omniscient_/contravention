// --custom comment-- mean comment written by developer
// because as a developer, i use intelij which automatically comment line

import 'package:contravention/pages/home/payment_succes.dart';
import 'package:contravention/reUsable/slide_left_to_right.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:contravention/reUsable/constants_variables.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';


enum PayMethod { myAccount, tresorPay, orange, wave }

class FeedYourAccount extends StatefulWidget {
  const FeedYourAccount({super.key});

  @override
  State<FeedYourAccount> createState() => _FeedYourAccountState();
}

class _FeedYourAccountState extends State<FeedYourAccount> {

  List<Color?> borderColor = List.generate(4, (_) => null);
  late TextEditingController phoneTextFieldController;
  late TextEditingController amountTextFieldController;
  List<TextEditingController> otpDigitControllers =  List.generate(4, (_) => TextEditingController());
  List<FocusNode> focusNodeList = List.generate(4, (_) => FocusNode());
  PayMethod? _character;
  
  Widget? nameFieldSuffix;
  Widget? phoneFieldSuffix;

  @override
  void initState() {
    phoneTextFieldController = TextEditingController();
    amountTextFieldController = TextEditingController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: primaryBackground,
        leading: const Icon(FontAwesomeIcons.xmark, color: Colors.transparent,),
      ),
      backgroundColor: primaryBackground,
      body: SingleChildScrollView(
        // --Custom comment-- we use Stack here because we want
        // to freely move the avatar
        child: Stack(
          children: [
            Column(
              children: [
                Center(
                  child: Row(
                    children: [
                      IconButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        icon: const Icon(FontAwesomeIcons.xmark, size: 16,),
                      ),

                    ],
                  ),
                ),
                SizedBox(height: screenHeight * 0.068,),
                Container(
                  width: screenWidth,
                  height: screenHeight,
                  decoration: const BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(40),
                          topRight: Radius.circular(40)
                      )
                  ),
                  child: Center(
                    child: Column(
                      children: [
                        const SizedBox(height: 70,),
                        const Text(
                          "Ajoutez de l'argent à",
                          style: TextStyle(
                              fontWeight: FontWeight.w600,
                              fontFamily: primaryTextFont, color: textColor,
                              fontSize: 18,
                              letterSpacing: 0.5
                          ),),
                        const Text(
                          'votre portefeuille électronique',
                            style: TextStyle(
                                fontWeight: FontWeight.w600,
                                fontFamily: primaryTextFont, color: textColor,
                                fontSize: 18,
                                letterSpacing: 0.5
                            )
                        ),
                        const SizedBox(height: 60,),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: screenWidth * 0.06),
                          child: SizedBox(
                            height: 85,
                            child: CupertinoTextField(
                              onChanged: (value) {
                                setState(() {
                                  if(value.length >= 2 && RegExp(r'^(01|05|07)[0-9]{8}$').hasMatch(value)) {
                                    phoneFieldSuffix = Row(
                                      children: [
                                        Container(
                                          width: 22,
                                          height: 22,
                                          decoration: BoxDecoration(
                                              shape: BoxShape.circle,
                                              border: Border.all(width: 2, color: primaryGreenColor)
                                          ),
                                          child: const Icon(Icons.check, color: primaryGreenColor, size: 15,),
                                        ),
                                        const SizedBox(width: 10,),
                                      ],
                                    );
                                  } else {
                                    phoneFieldSuffix = null;
                                  }
                                });
                              },
                              controller: phoneTextFieldController,
                              prefix: Column(
                                children: [
                                  const SizedBox(height: 7,),
                                  const Row(
                                    children: [
                                      SizedBox(width: 10,),
                                      Text('Numéro de téléphone',
                                        style: TextStyle(
                                          fontFamily: primaryTextFont,
                                          color: greyColor,
                                        ),
                                      ),
                                    ],
                                  ),
                                  const SizedBox(height: 1,),
                                  Row(
                                    children: [
                                      // const SizedBox(width: 10,),
                                      Container(
                                        width: 40,
                                        height: 40,
                                        decoration: const BoxDecoration(
                                            shape: BoxShape.circle,
                                            image: DecorationImage(
                                                image: AssetImage('assets/imgs/ivoryCoast_flag.png')
                                            )
                                        ),
                                      ),
                                      const SizedBox(width: 10,),
                                      const Text('(+225)', style: TextStyle(color: textColor,
                                          fontFamily: primaryTextFont, fontSize: 14),),
                                      const SizedBox(width: 20,)
                                    ],
                                  ),
                                ],
                              ),
                              suffix: phoneFieldSuffix,
                              placeholder: 'numéro de téléphone',
                              keyboardType: TextInputType.phone,
                              decoration: BoxDecoration(
                                  color: textFieldBackground,
                                  borderRadius: BorderRadius.circular(15)
                              ),
                            ),
                          ),
                        ),

                        const SizedBox(height: 30,),

                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: screenWidth * 0.06),
                          child: Row(
                            children: [
                              SizedBox(
                                height: 60,
                                width: screenWidth * 0.7,
                                child: CupertinoTextField(
                                  controller: amountTextFieldController,
                                  prefix: const Column(
                                    children: [
                                      SizedBox(height: 7,),
                                      Row(
                                        children: [
                                          SizedBox(width: 10,),
                                          Text('Montant',
                                            style: TextStyle(
                                              fontFamily: primaryTextFont,
                                              color: greyColor,
                                            ),
                                          ),
                                        ],
                                      ),

                                    ],
                                  ),
                                  keyboardType: TextInputType.number,
                                  decoration: BoxDecoration(
                                      color: textFieldBackground,
                                      borderRadius: BorderRadius.circular(15)
                                  ),
                                ),
                              ),
                              SizedBox(width: screenWidth * 0.03,),
                              const Text(
                                'FCFA',
                                style: TextStyle(
                                  fontSize: 20,
                                  fontWeight: FontWeight.w600
                                ),
                              )
                            ],
                          ),
                        ),
                        const SizedBox(height: 50,),
                        ListTile(
                          title: const Text('Tresor Pay'),
                          leading: Container(
                            width: 50,
                            height: 50,
                            decoration: const BoxDecoration(
                                shape: BoxShape.circle,
                                image: DecorationImage(
                                    image: AssetImage('assets/imgs/tresorPay.png')
                                )
                            ),
                          ),
                          trailing:  Radio<PayMethod>(
                            value: PayMethod.tresorPay,
                            groupValue: _character,
                            onChanged: (PayMethod? value) {
                              setState(() {
                                _character = value;
                              });
                            },
                          ),),
                        ListTile(
                          title: const Text('Orange money'),
                          leading: Container(
                            width: 50,
                            height: 50,
                            decoration: const BoxDecoration(
                                shape: BoxShape.circle,
                                image: DecorationImage(
                                    image: AssetImage('assets/imgs/orangeMoney.jpeg')
                                )
                            ),
                          ),
                          trailing:  Radio<PayMethod>(
                            value: PayMethod.orange,
                            groupValue: _character,
                            onChanged: (PayMethod? value) {
                              setState(() {
                                _character = value;
                              });
                            },
                          ),),
                        ListTile(
                          title: const Text('Wave'),
                          leading: Container(
                            width: 50,
                            height: 50,
                            decoration: const BoxDecoration(
                                shape: BoxShape.circle,
                                image: DecorationImage(
                                    image: AssetImage('assets/imgs/wave.jpeg')
                                )
                            ),
                          ),
                          trailing:  Radio<PayMethod>(
                            value: PayMethod.wave,
                            groupValue: _character,
                            onChanged: (PayMethod? value) {
                              setState(() {
                                _character = value;
                              });
                            },
                          ),),
                        const Spacer(),
                        Container(
                          width: screenWidth * 0.82,
                          height: 50,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(15),
                              color: primaryGreenColor
                          ),
                          child: TextButton(
                            onPressed: () {
                              bool err = false;
                              String errTxt = '';
                              if (phoneFieldSuffix == null) {
                                err = true;
                                errTxt = 'veuillez bien renseigner le champs Téléphone';
                              } else if(amountTextFieldController.text.isEmpty){
                                err = true;
                                errTxt = 'veuillez bien renseigner le champs Montant';
                              } else if(_character == null) {
                                err = true;
                                errTxt = 'Veuillez choisir un moyen de paiement';
                              }

                              if (err) {
                                ScaffoldMessenger.of(context).showSnackBar(
                                    SnackBar(
                                      content: Text(
                                        ' $errTxt',
                                        style: const TextStyle(
                                            color: Colors.white),),
                                      backgroundColor: Colors.red,
                                    )
                                );
                              } else {
                                showDialog(
                                    context: context,
                                    builder: (BuildContext context) => Dialog(
                                      backgroundColor: Colors.transparent,
                                      insetPadding: const EdgeInsets.all(10.0),
                                      elevation: 0,
                                      shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(20.0)
                                      ),
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                        children: [
                                          Container(
                                            decoration: BoxDecoration(
                                                borderRadius: BorderRadius.circular(20.0),
                                                color: Colors.white
                                            ),
                                            child:  Padding(
                                              padding: EdgeInsets.symmetric(horizontal: screenWidth * 0.03, vertical: 34),
                                              child: Column(
                                                children: [
                                                  Container(
                                                    width: 80,
                                                    height: 80,
                                                    decoration: BoxDecoration(
                                                      border: Border.all(width: 4, color: greyColor),
                                                      shape: BoxShape.circle,
                                                    ),
                                                    child: const Icon(Icons.phonelink_ring, size: 40,),
                                                  ),
                                                  const SizedBox(height: 40,),
                                                  const Text(
                                                      "Tapez #144*82#, entrez votre code ",
                                                      style: TextStyle(
                                                        color: greyColor,
                                                        fontSize: 18,
                                                      )
                                                  ),
                                                  const Text(
                                                      "secret et entrez ici le code ",
                                                      style: TextStyle(
                                                        color: greyColor,
                                                        fontSize: 18,
                                                      )
                                                  ),
                                                  const Text(
                                                      "temporaire",
                                                      style: TextStyle(
                                                        color: greyColor,
                                                        fontSize: 18,
                                                      )
                                                  ),
                                                  const SizedBox(height: 30,),
                                                  Row(
                                                    children: [
                                                      Container(
                                                        padding: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 15.0),
                                                        width: 70,
                                                        height: 70,
                                                        decoration: BoxDecoration(color: textFieldBackground,
                                                          borderRadius: BorderRadius.circular(15.0),
                                                          border: Border.all(width: 1, color: borderColor[0] == null ? Colors.transparent : borderColor[0]!),
                                                        ),
                                                        child: Center(
                                                          child: CupertinoTextField(
                                                            padding: EdgeInsets.symmetric(horizontal: screenWidth * 0.03),
                                                            focusNode: focusNodeList[0],
                                                            maxLength: 1,
                                                            cursorColor: primaryGreenColor,
                                                            keyboardType: TextInputType.number,
                                                            style: const TextStyle(
                                                              fontSize: 20,
                                                              fontFamily: primaryTextFont,
                                                              color: textColor,
                                                              fontWeight: FontWeight.w600,
                                                            ),
                                                            decoration: const BoxDecoration(
                                                              color: Colors.transparent,

                                                            ),
                                                            controller: otpDigitControllers[0],
                                                            onChanged: (String value) {
                                                              if(value.isNotEmpty) {
                                                                focusNodeList[1].requestFocus();
                                                                setState(() {
                                                                  borderColor[0] = primaryGreenColor;
                                                                });
                                                              } else {
                                                                setState(() {
                                                                  borderColor[0] = null;
                                                                });
                                                              }
                                                            },
                                                          ),
                                                        ),
                                                      ),
                                                      SizedBox(width: screenWidth * 0.03,),
                                                      Container(
                                                        padding: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 15.0),
                                                        width: 70,
                                                        height: 70,
                                                        decoration: BoxDecoration(
                                                          color: textFieldBackground,
                                                          borderRadius: BorderRadius.circular(15.0),
                                                          border: Border.all(width: 1, color: borderColor[1] == null ? Colors.transparent : borderColor[1]!),
                                                        ),
                                                        child: Center(
                                                          child: CupertinoTextField(
                                                            padding: EdgeInsets.symmetric(horizontal: screenWidth * 0.03),
                                                            focusNode: focusNodeList[1],
                                                            maxLength: 1,
                                                            cursorColor: primaryGreenColor,
                                                            keyboardType: TextInputType.number,
                                                            style: const TextStyle(
                                                              fontSize: 20,
                                                              fontFamily: primaryTextFont,
                                                              color: textColor,
                                                              fontWeight: FontWeight.w600,
                                                            ),
                                                            decoration: const BoxDecoration(
                                                              color: Colors.transparent,

                                                            ),
                                                            controller: otpDigitControllers[1],
                                                            onChanged: (String value) {
                                                              if(value.isNotEmpty) {
                                                                focusNodeList[2].requestFocus();
                                                                setState(() {
                                                                  borderColor[1] = primaryGreenColor;
                                                                });
                                                              } else {
                                                                setState(() {
                                                                  borderColor[1] = null;
                                                                });
                                                              }
                                                            },
                                                          ),
                                                        ),
                                                      ),
                                                      SizedBox(width: screenWidth * 0.03,),
                                                      Container(
                                                        padding: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 15.0),
                                                        width: 70,
                                                        height: 70,
                                                        decoration: BoxDecoration(
                                                          color: textFieldBackground,
                                                          borderRadius: BorderRadius.circular(15.0),
                                                          border: Border.all(width: 1, color: borderColor[2] == null ? Colors.transparent : borderColor[2]!),
                                                        ),
                                                        child: Center(
                                                          child: CupertinoTextField(
                                                            padding: EdgeInsets.symmetric(horizontal: screenWidth * 0.03),
                                                            focusNode: focusNodeList[2],
                                                            maxLength: 1,
                                                            cursorColor: primaryGreenColor,
                                                            keyboardType: TextInputType.number,
                                                            style: const TextStyle(
                                                              fontSize: 20,
                                                              fontFamily: primaryTextFont,
                                                              color: textColor,
                                                              fontWeight: FontWeight.w600,
                                                            ),
                                                            decoration: const BoxDecoration(
                                                              color: Colors.transparent,

                                                            ),
                                                            controller: otpDigitControllers[2],
                                                            onChanged: (String value) {
                                                              if(value.isNotEmpty) {
                                                                focusNodeList[3].requestFocus();
                                                                setState(() {
                                                                  borderColor[2] = primaryGreenColor;
                                                                });
                                                              } else {
                                                                setState(() {
                                                                  borderColor[2] = null;
                                                                });
                                                              }
                                                            },
                                                          ),
                                                        ),
                                                      ),
                                                      SizedBox(width: screenWidth * 0.03,),
                                                      Container(
                                                        padding: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 15.0),
                                                        width: 70,
                                                        height: 70,
                                                        decoration: BoxDecoration(
                                                          color: textFieldBackground,
                                                          borderRadius: BorderRadius.circular(15.0),
                                                          border: Border.all(width: 1, color: borderColor[3] == null ? Colors.transparent : borderColor[3]!),
                                                        ),
                                                        child: Center(
                                                          child: CupertinoTextField(
                                                            padding: EdgeInsets.symmetric(horizontal: screenWidth * 0.03),
                                                            focusNode: focusNodeList[3],
                                                            maxLength: 1,
                                                            cursorColor: primaryGreenColor,
                                                            keyboardType: TextInputType.number,
                                                            style: const TextStyle(
                                                              fontSize: 20,
                                                              fontFamily: primaryTextFont,
                                                              color: textColor,
                                                              fontWeight: FontWeight.w600,
                                                            ),
                                                            decoration: const BoxDecoration(
                                                              color: Colors.transparent,

                                                            ),
                                                            controller: otpDigitControllers[3],
                                                            onChanged: (String value) {
                                                              if(value.isNotEmpty) {
                                                                focusNodeList[3].unfocus();
                                                                borderColor[3] = primaryGreenColor;
                                                              } else {
                                                                setState(() {
                                                                  borderColor[3] = null;
                                                                });
                                                              }
                                                            },
                                                          ),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                          const SizedBox(height: 30,),
                                          Container(
                                            width: screenWidth * 0.82,
                                            height: 50,
                                            decoration: BoxDecoration(
                                                borderRadius: BorderRadius.circular(15),
                                                color: yellowColor
                                            ),
                                            child: TextButton(
                                              onPressed: () {
                                                Navigator.push(context, SlideRightPageRoute(page: const PaymentSuccess(type: 'payment',)));
                                              },
                                              child: const Text(
                                                'Valider le payement',
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 20,
                                                    shadows: [
                                                      BoxShadow(
                                                          spreadRadius: 0.5,
                                                          blurRadius: 0.5,
                                                          color: Colors.black
                                                      )
                                                    ]
                                                ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    )
                                );
                              }
                            },
                            child: const Text(
                              'CONTINUER',
                              style: TextStyle(
                                  letterSpacing: 0.5,
                                  fontSize: 16,
                                  fontFamily: primaryTextFont,
                                  color: Colors.white
                              ),
                            ),
                          ),
                        ),

                        const SizedBox(height: 20,),
                      ],
                    ),
                  ),
                )
              ],
            ),
            Positioned(
              width: 80,
              height: 80,
              left: screenWidth * 0.39,
              top: screenHeight * 0.074,
              child: const CircleAvatar(
                backgroundColor: primaryCircleAvatarColor,
                child: Icon(Icons.wallet, size: 50,),
              ),

            ),
          ],
        ),
      ),
    );
  }


}