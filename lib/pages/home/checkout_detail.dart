// --custom comment-- mean comment written by developer
// because as a developer, i use intelij which automatically comment line

import 'package:contravention/pages/home/payment_succes.dart';
import 'package:contravention/reUsable/slide_left_to_right.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:contravention/reUsable/constants_variables.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

enum CheckOutMethod { myAccount, tresorPay, orange, wave }

class CheckoutDetails extends StatefulWidget {
   const CheckoutDetails({
    super.key,
    required this.title, required this.name,
    required this.role, required this.photo, required
     this.infraction, required this.carNumber, required this.price,
     required this.amount});

  final String title;
  final String name;
  final String role;
  final String photo;
  final String infraction;
  final String carNumber;
  final String price;
  final String amount;

  @override
  State<CheckoutDetails> createState() => _CheckoutDetailsState();
}

class _CheckoutDetailsState extends State<CheckoutDetails> {


  List<Color?> borderColor = List.generate(4, (_) => null);
  CheckOutMethod? _character = CheckOutMethod.myAccount;
  List<TextEditingController> otpDigitControllers =  List.generate(4, (_) => TextEditingController());
  List<FocusNode> focusNodeList = List.generate(4, (_) => FocusNode());

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: primaryBackground,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: const Icon(FontAwesomeIcons.xmark, size: 16, color: Colors.black,),
        ),
      ),
      backgroundColor: primaryBackground,
      body: SingleChildScrollView(
        // --Custom comment-- we use Stack here because we want
        // to freely move the avatar
        child: Column(
          children: [
            const Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                SizedBox(width: 50,),
                Column(
                  children: [
                    Text(
                      'Payer',
                      style: TextStyle(
                        color: primaryGreenColor,
                        fontSize: 30,
                      ),
                    ),
                    Text(
                      'Détails de la contravention',
                      style: TextStyle(
                        fontSize: 20,
                      ),
                    )
                  ],
                ),
              ],
            ),
            // const SizedBox(height: 0,),
            Column(
              children: [
                SizedBox(height: screenHeight * 0.05,),
                Container(
                  decoration: const BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(40),
                          topRight: Radius.circular(40)
                      )
                  ),
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(top: 24.0),
                        child: Text(
                          widget.title,
                          style: const TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.w600,
                            color: textColor,
                          )
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(20.0),
                        child: Container(
                          padding: const EdgeInsets.all(20.0),
                          decoration: BoxDecoration(
                            border: Border.all(width: 1, color: textColor),
                            borderRadius: BorderRadius.circular(13),
                          ),
                          child: ListTile(
                            leading: Container(
                              width: 55,
                              height: 55,
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                  image: AssetImage(widget.photo)
                                ),
                                borderRadius: BorderRadius.circular(15)
                              ),
                            ),
                            title: Text(
                              widget.role.toUpperCase(),
                              style: const TextStyle(
                                fontSize: 14,
                                color: greyColor,
                              )
                            ),
                            subtitle: Text(
                              widget.name,
                              style: const TextStyle(
                                fontSize: 21,
                                fontWeight: FontWeight.w600,
                                fontFamily: primaryTextFont,
                                color: Colors.black
                              )
                            )
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                const SizedBox(height: 30,),
                const Padding(
                  padding: EdgeInsets.all(10.0),
                  child: Row(
                    children: [
                      Text(
                        'DETAIL',
                        style: TextStyle(
                          fontWeight: FontWeight.w600,
                          color: primaryGreenColor,
                          fontSize: 20
                        )
                      )
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Row(
                    children: [
                      const Text(
                          'Infraction',
                          style: TextStyle(
                            color: Colors.grey,
                            fontSize: 15
                          )
                      ),
                      const Spacer(),
                      Text(
                          widget.infraction,
                          style: const TextStyle(
                            color: Colors.grey,
                          )
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Row(
                    children: [
                      const Text(
                          'Immatriculation',
                          style: TextStyle(
                            color: Colors.grey,
                          )
                      ),
                      const Spacer(),
                      Text(
                          widget.carNumber,
                          style: const TextStyle(
                            color: Colors.grey,
                          )
                      ),
                    ],
                  ),
                ),
                Container(
                  width: screenWidth,
                  height: 2,
                  color: greyColor,
                ),
                const SizedBox(height: 30,),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    children: [
                      Row(
                        children: [
                          const Text(
                              'A Payer',
                              style: TextStyle(
                                  color: primaryGreenColor,
                                  fontSize: 18,
                                  fontWeight: FontWeight.w600
                              )
                          ),
                          const Spacer(),
                          Text(
                              '${widget.price} FCFA',
                              style: const TextStyle(
                                  color: primaryGreenColor,
                                  fontSize: 18,
                                  fontWeight: FontWeight.w600
                              )
                          )
                        ]
                      ),
                      const SizedBox(height: 25),
                      Container(
                        padding: const EdgeInsets.all(10.0),
                        width: screenWidth,
                        height: screenHeight * 0.7,
                        color: Colors.white,
                        child: Column(
                          children: [
                            const Row(
                              children: [
                                SizedBox(height: 70,),
                                Text('Payer avec',
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.w600,
                                    fontSize: 20,
                                  )
                                )
                              ],
                            ),
                            ListTile(
                              title: const Text('Mon portefeuille'),
                              subtitle: Text(
                                '${widget.amount} FCFA',
                              ),
                              leading: Container(
                                width: 50,
                                height: 50,
                                decoration: const BoxDecoration(
                                  shape: BoxShape.circle,
                                  image: DecorationImage(
                                    image: AssetImage('assets/imgs/myAccount.png')
                                  )
                                ),
                              ),
                              trailing:  Radio<CheckOutMethod>(
                                  value: CheckOutMethod.myAccount,
                                  groupValue: _character,
                                  onChanged: (CheckOutMethod? value) {
                                    setState(() {
                                      _character = value;
                                    });
                                  },
                            ),),
                            ListTile(
                              title: const Text('Tresor Pay'),
                              leading: Container(
                                width: 50,
                                height: 50,
                                decoration: const BoxDecoration(
                                    shape: BoxShape.circle,
                                    image: DecorationImage(
                                        image: AssetImage('assets/imgs/tresorPay.png')
                                    )
                                ),
                              ),
                              trailing:  Radio<CheckOutMethod>(
                                value: CheckOutMethod.tresorPay,
                                groupValue: _character,
                                onChanged: (CheckOutMethod? value) {
                                  setState(() {
                                    _character = value;
                                  });
                                },
                              ),),
                            ListTile(
                              title: const Text('Orange money'),
                              leading: Container(
                                width: 50,
                                height: 50,
                                decoration: const BoxDecoration(
                                    shape: BoxShape.circle,
                                    image: DecorationImage(
                                        image: AssetImage('assets/imgs/orangeMoney.jpeg')
                                    )
                                ),
                              ),
                              trailing:  Radio<CheckOutMethod>(
                                value: CheckOutMethod.orange,
                                groupValue: _character,
                                onChanged: (CheckOutMethod? value) {
                                  setState(() {
                                    _character = value;
                                  });
                                },
                              ),),
                            ListTile(
                              title: const Text('Wave'),
                              leading: Container(
                                width: 50,
                                height: 50,
                                decoration: const BoxDecoration(
                                    shape: BoxShape.circle,
                                    image: DecorationImage(
                                        image: AssetImage('assets/imgs/wave.jpeg')
                                    )
                                ),
                              ),
                              trailing:  Radio<CheckOutMethod>(
                                value: CheckOutMethod.wave,
                                groupValue: _character,
                                onChanged: (CheckOutMethod? value) {
                                  setState(() {
                                    _character = value;
                                  });
                                },
                              ),),
                            const Spacer(),
                            Container(
                              width: screenWidth * 0.82,
                              height: 50,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(15),
                                  color: primaryGreenColor
                              ),
                              child: TextButton(

                                onPressed: () {

                                  showDialog(
                                    context: context,
                                    builder: (BuildContext context) => Dialog(
                                      backgroundColor: Colors.transparent,
                                      insetPadding: const EdgeInsets.all(10.0),
                                      elevation: 0,
                                     shape: RoundedRectangleBorder(
                                       borderRadius: BorderRadius.circular(20.0)
                                     ),
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                        children: [
                                          Container(
                                            decoration: BoxDecoration(
                                              borderRadius: BorderRadius.circular(20.0),
                                              color: Colors.white
                                            ),
                                            child:  Padding(
                                              padding: EdgeInsets.symmetric(horizontal: screenWidth * 0.03, vertical: 34),
                                              child: Column(
                                                children: [
                                                  Container(
                                                    width: 80,
                                                    height: 80,
                                                    decoration: BoxDecoration(
                                                      border: Border.all(width: 4, color: greyColor),
                                                      shape: BoxShape.circle,
                                                    ),
                                                    child: const Icon(Icons.phonelink_ring, size: 40,),
                                                  ),
                                                  const SizedBox(height: 40,),
                                                  const Text(
                                                      "Tapez #144*82#, entrez votre code ",
                                                      style: TextStyle(
                                                        color: greyColor,
                                                        fontSize: 18,
                                                      )
                                                  ),
                                                  const Text(
                                                      "secret et entrez ici le code ",
                                                      style: TextStyle(
                                                        color: greyColor,
                                                        fontSize: 18,
                                                      )
                                                  ),
                                                  const Text(
                                                      "temporaire",
                                                      style: TextStyle(
                                                        color: greyColor,
                                                        fontSize: 18,
                                                      )
                                                  ),
                                                  const SizedBox(height: 30,),
                                                  Row(
                                                    mainAxisAlignment: MainAxisAlignment.start,
                                                    children: [
                                                      Container(
                                                        padding: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 15.0),
                                                        width: 70,
                                                        height: 70,
                                                        decoration: BoxDecoration(color: textFieldBackground,
                                                          borderRadius: BorderRadius.circular(15.0),
                                                          border: Border.all(width: 1, color: borderColor[0] == null ? Colors.transparent : borderColor[0]!),
                                                        ),
                                                        child: Center(
                                                          child: CupertinoTextField(
                                                            padding: EdgeInsets.symmetric(horizontal: screenWidth * 0.03),
                                                            focusNode: focusNodeList[0],
                                                            maxLength: 1,
                                                            cursorColor: primaryGreenColor,
                                                            keyboardType: TextInputType.number,
                                                            style: const TextStyle(
                                                              fontSize: 20,
                                                              fontFamily: primaryTextFont,
                                                              color: textColor,
                                                              fontWeight: FontWeight.w600,
                                                            ),
                                                            decoration: const BoxDecoration(
                                                              color: Colors.transparent,

                                                            ),
                                                            controller: otpDigitControllers[0],
                                                            onChanged: (String value) {
                                                              if(value.isNotEmpty) {
                                                                focusNodeList[1].requestFocus();
                                                                setState(() {
                                                                  borderColor[0] = primaryGreenColor;
                                                                });
                                                              } else {
                                                                setState(() {
                                                                  borderColor[0] = null;
                                                                });
                                                              }
                                                            },
                                                          ),
                                                        ),
                                                      ),
                                                      SizedBox(width: screenWidth * 0.03,),
                                                      Container(
                                                        padding: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 15.0),
                                                        width: 70,
                                                        height: 70,
                                                        decoration: BoxDecoration(
                                                          color: textFieldBackground,
                                                          borderRadius: BorderRadius.circular(15.0),
                                                          border: Border.all(width: 1, color: borderColor[1] == null ? Colors.transparent : borderColor[1]!),
                                                        ),
                                                        child: Center(
                                                          child: CupertinoTextField(
                                                            padding: EdgeInsets.symmetric(horizontal: screenWidth * 0.03),
                                                            focusNode: focusNodeList[1],
                                                            maxLength: 1,
                                                            cursorColor: primaryGreenColor,
                                                            keyboardType: TextInputType.number,
                                                            style: const TextStyle(
                                                              fontSize: 20,
                                                              fontFamily: primaryTextFont,
                                                              color: textColor,
                                                              fontWeight: FontWeight.w600,
                                                            ),
                                                            decoration: const BoxDecoration(
                                                              color: Colors.transparent,

                                                            ),
                                                            controller: otpDigitControllers[1],
                                                            onChanged: (String value) {
                                                              if(value.isNotEmpty) {
                                                                focusNodeList[2].requestFocus();
                                                                setState(() {
                                                                  borderColor[1] = primaryGreenColor;
                                                                });
                                                              } else {
                                                                setState(() {
                                                                  borderColor[1] = null;
                                                                });
                                                              }
                                                            },
                                                          ),
                                                        ),
                                                      ),
                                                      SizedBox(width: screenWidth * 0.03,),
                                                      Container(
                                                        padding: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 15.0),
                                                        width: 70,
                                                        height: 70,
                                                        decoration: BoxDecoration(
                                                          color: textFieldBackground,
                                                          borderRadius: BorderRadius.circular(15.0),
                                                          border: Border.all(width: 1, color: borderColor[2] == null ? Colors.transparent : borderColor[2]!),
                                                        ),
                                                        child: Center(
                                                          child: CupertinoTextField(
                                                            padding: EdgeInsets.symmetric(horizontal: screenWidth * 0.03),
                                                            focusNode: focusNodeList[2],
                                                            maxLength: 1,
                                                            cursorColor: primaryGreenColor,
                                                            keyboardType: TextInputType.number,
                                                            style: const TextStyle(
                                                              fontSize: 20,
                                                              fontFamily: primaryTextFont,
                                                              color: textColor,
                                                              fontWeight: FontWeight.w600,
                                                            ),
                                                            decoration: const BoxDecoration(
                                                              color: Colors.transparent,

                                                            ),
                                                            controller: otpDigitControllers[2],
                                                            onChanged: (String value) {
                                                              if(value.isNotEmpty) {
                                                                focusNodeList[3].requestFocus();
                                                                setState(() {
                                                                  borderColor[2] = primaryGreenColor;
                                                                });
                                                              } else {
                                                                setState(() {
                                                                  borderColor[2] = null;
                                                                });
                                                              }
                                                            },
                                                          ),
                                                        ),
                                                      ),
                                                      SizedBox(width: screenWidth * 0.03,),
                                                      Container(
                                                        padding: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 15.0),
                                                        width: 70,
                                                        height: 70,
                                                        decoration: BoxDecoration(
                                                          color: textFieldBackground,
                                                          borderRadius: BorderRadius.circular(15.0),
                                                          border: Border.all(width: 1, color: borderColor[3] == null ? Colors.transparent : borderColor[3]!),
                                                        ),
                                                        child: Center(
                                                          child: CupertinoTextField(
                                                            padding: EdgeInsets.symmetric(horizontal: screenWidth * 0.03),
                                                            focusNode: focusNodeList[3],
                                                            maxLength: 1,
                                                            cursorColor: primaryGreenColor,
                                                            keyboardType: TextInputType.number,
                                                            style: const TextStyle(
                                                              fontSize: 20,
                                                              fontFamily: primaryTextFont,
                                                              color: textColor,
                                                              fontWeight: FontWeight.w600,
                                                            ),
                                                            decoration: const BoxDecoration(
                                                              color: Colors.transparent,

                                                            ),
                                                            controller: otpDigitControllers[3],
                                                            onChanged: (String value) {
                                                              if(value.isNotEmpty) {
                                                                focusNodeList[3].unfocus();
                                                                borderColor[3] = primaryGreenColor;
                                                              } else {
                                                                setState(() {
                                                                  borderColor[3] = null;
                                                                });
                                                              }
                                                            },
                                                          ),
                                                        ),
                                                      ),

                                                    ],
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                          const SizedBox(height: 30,),
                                          Container(
                                            width: screenWidth * 0.82,
                                            height: 50,
                                            decoration: BoxDecoration(
                                                borderRadius: BorderRadius.circular(15),
                                                color: yellowColor
                                            ),
                                            child: TextButton(
                                              onPressed: () {
                                                Navigator.push(context, SlideRightPageRoute(page: const PaymentSuccess(type: 'checkcout',)));
                                              },
                                              child: const Text(
                                                'Valider le payement',
                                                style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 20,
                                                  shadows: [
                                                    BoxShadow(
                                                      spreadRadius: 0.5,
                                                      blurRadius: 0.5,
                                                      color: Colors.black
                                                    )
                                                  ]
                                                ),
                                              ),
                                            ),
                                          ),

                                        ],
                                      ),
                                    )
                                  );
                                },
                                child: const Text(
                                  'PAYER LA CONTRAVENTION',
                                  style: TextStyle(
                                      letterSpacing: 0.5,
                                      fontSize: 16,
                                      fontFamily: primaryTextFont,
                                      color: Colors.white
                                  ),
                                )
                              )
                            ),
                            const SizedBox(height: 30),
                          ],
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),

          ],
        ),
      ),
    );
  }


}